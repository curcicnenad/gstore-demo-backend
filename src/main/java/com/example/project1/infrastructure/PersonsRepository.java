package com.example.project1.infrastructure;

import com.example.project1.infrastructure.domen.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonsRepository extends JpaRepository<Person, Long> {

}
