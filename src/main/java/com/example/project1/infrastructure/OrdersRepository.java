package com.example.project1.infrastructure;

import com.example.project1.infrastructure.domen.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdersRepository extends JpaRepository<Order, Long>{

}
