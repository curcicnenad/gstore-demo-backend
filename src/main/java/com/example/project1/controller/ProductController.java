package com.example.project1.controller;

import com.example.project1.service.ProductService;
import com.example.project1.service.ProductServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@RequestMapping("/products")
public class ProductController {
    final Logger log = LoggerFactory.getLogger(ProductController.class);

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService){
        this.productService= productService;
    }

    @GetMapping(value = "/{categoryName}")
    public ResponseEntity getProducts(@PathVariable("categoryName") String categoryName){
        log.info("Primljen get request");
        return new ResponseEntity(productService.getProducts(categoryName), HttpStatus.OK);
    }

    @GetMapping(value = "/persons")
    public ResponseEntity getAllPersons(){
        log.info("Primljen get request");
        return new ResponseEntity(productService.getPersons(), HttpStatus.OK);
    }

}
