package com.example.project1.service;

import com.example.project1.infrastructure.PersonsRepository;
import com.example.project1.infrastructure.domen.Person;
import com.example.project1.model.Product;
import com.example.project1.model.ResponseProducts;
import com.example.project1.thirdparty.ProductServiceApiClient;
import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
    private ProductServiceApiClient productServiceApiClient;

    private PersonsRepository personsRepository;


    private static final String AUTH_KEY = "qdaiciDiyMaTjxMt, 74026b3dc2c6db6a30a73e71cdb138b1e1b5eb7a97ced46689e2d28db1050875";

    @Autowired
    public ProductServiceImpl(ProductServiceApiClient productServiceApiClient, PersonsRepository personsRepository) {

        this.productServiceApiClient = productServiceApiClient;
        this.personsRepository = personsRepository;

    }


    @Override
    public List<Product> getProducts(String categoryName) {

        try {
          //  ResponseProducts listAll = new ResponseProducts();
            List<Product> allList = new ArrayList<Product>();

           //    log.info("prefetched page category: " + categoryName);
//                ResponseProducts response1 = productServiceApiClient.getProducts(AUTH_KEY,1).execute().body();

            //List<Product> nesto = resultProductsList(callAPI(1).docs, categoryName);
            List<Product> nesto = new ArrayList<Product>();
            int i = 1;
            while(nesto.size()<=5 && (i <= 500)){
                    log.info("new request " + i);
                    nesto.addAll(resultProductsList(callAPI(i).docs,categoryName));
            i++;
            }


            return nesto;
        }
        catch (Exception e){
           log.error(e.getMessage());
           return null;
        }

    }
    private ResponseProducts callAPI(int page){
        try {
            return productServiceApiClient.getProducts(AUTH_KEY,page).execute().body();
        }catch (Exception e){
            log.error(e.getMessage());
            return null;
        }
    }



    private boolean checkProduct (Product product, String searchingCategory) {

       // log.info("product name: " + product.getName());
        boolean ok = false;
        int i = 0;
       // log.info("number of categories: " + product.getCategories().size());
        if (product.getCategories().size() != 0 ) {
            while (!ok && i < product.getCategories().size()) {
             //   log.info("category checked " + product.getCategories().get(i).getName());
                if (product.getCategories().get(i).getName().equals(searchingCategory)) {
                    //log.info("Product FOUND");
                    ok = true;
                }
                i++;
            }
        }
        return ok;
    }

    private List<Product> resultProductsList(List<Product> products, String searchCategoryName) {
        List<Product> result = new ArrayList<>();

        for (int i = 0; i < products.size(); i++) {
            if (checkProduct(products.get(i), searchCategoryName)) {
                //log.info("Product " + products.get(i).getName() + " added to list!");
                result.add(products.get(i));
            }
        }
        return result;
    }

    @Override
    public List<Person> getPersons() {
        return personsRepository.findAll();
    }


}
