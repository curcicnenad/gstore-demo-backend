package com.example.project1.service;

import com.example.project1.infrastructure.domen.Order;
import java.util.List;

public interface OrderService {
    List<Order> getOrdersHistory(Long userID);
}
