package com.example.project1.service;

import com.example.project1.infrastructure.domen.Person;
import com.example.project1.model.Product;
import java.util.List;
import com.example.project1.model.ResponseProducts;

public interface ProductService {

    List<Product> getProducts(String categoryName);

    List<Person> getPersons();

}
