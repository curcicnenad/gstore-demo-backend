package com.example.project1.service;

import com.example.project1.infrastructure.OrdersRepository;
import com.example.project1.infrastructure.domen.Order;
import lombok.extern.slf4j.Slf4j;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class OrderServiceImpl implements OrderService{

    private OrdersRepository ordersRepository;
    private EntityManager em;

    @Autowired
    public OrderServiceImpl(OrdersRepository ordersRepo, EntityManager entityManager){
        this.em = entityManager;
        this.ordersRepository = ordersRepo;
    }

    @Override
    public List<Order> getOrdersHistory(Long userID) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> cq = cb.createQuery(Order.class);

        Root<Order> order = cq.from(Order.class);
        Predicate userIDPredicate = cb.equal(order.get("userID"), userID);
        cq.where(userIDPredicate);

        TypedQuery<Order> query = em.createQuery(cq);
        return query.getResultList();
    }
}
