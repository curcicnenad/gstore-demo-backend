package com.example.project1.thirdparty;

import com.example.project1.model.ResponseProducts;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ProductServiceApiClient {

    @GET("products")
    @Headers("Content-Type: application/json")
    Call<ResponseProducts> getProducts(@Header("Authorization") String authorization, @Query("page") int page);

}
