package com.example.project1.thirdparty;

import com.fasterxml.jackson.databind.ObjectMapper;
import retrofit2.Retrofit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static retrofit2.converter.jackson.JacksonConverterFactory.create;

@Configuration
public class RemoteServiceConfiguration {

    @Value("${product.service.url}")
    private String productServiceBaseUrl;

    private ObjectMapper objectMapper;

    @Autowired
    public RemoteServiceConfiguration(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Bean
    public Retrofit productServiceRetrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(productServiceBaseUrl)
                .addConverterFactory(create(objectMapper))
                .build();
    }

    @Bean
    public ProductServiceApiClient userServiceApiClient() {
        return productServiceRetrofitBuilder().create(ProductServiceApiClient.class);
    }

}
